﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class Facturas : Form
    {
        private DataSet Factura;
        private DataSet dsSistemas;
        private BindingSource bsProducto;
        public Facturas()
        {
            InitializeComponent();
            bsProducto = new BindingSource();
        }

        public DataSet Factura1
        {
            get
            {
                return Factura;
            }

            set
            {
                Factura = value;
            }
        }

        public DataSet DsSistemas
        {

            set
            {
                dsSistemas = value;
            }
        }

        private void btnver_Click(object sender, EventArgs e)
        {
            InformacionFac ifa = new InformacionFac();
            ifa.MdiParent = this.MdiParent;
            //frf.DsSistema = dsSistema;
            ifa.Show();
        }

        private void Facturas_Load(object sender, EventArgs e)
        {
            cmbEmpleado = dsSistemas.Tables["ReporteFactura"];
            cmbEmpleado.DisplayMember = "nombre_Empleado";
            cmbEmpleado.ValueMember = "Cod_Facturas";

            dsSistemas.Tables["FiltroFactura"].Rows.Clear();
            bsProducto.DataSource = dsSistemas.Tables["FiltroFactura"];
            dataGridView1.DataSource = bsProducto;
        }

        private void cmbEmpleado_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}
